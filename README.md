# ContactsApp

[![pipeline status](https://gitlab.com/ssaczkowski/contactsapp/badges/master/pipeline.svg)](https://gitlab.com/ssaczkowski/contactsapp/commits/master)

Solstice Challenge

Last version Android SO. Pie 9

Frameworks
*  Picasso
*  ButterKnife

![](https://gitlab.com/ssaczkowski/contactsapp/uploads/14b6ac593d075aa6c04366983cd0bd97/Screen_Shot_2019-05-29_at_4.44.29_PM.png)


App that fetches JSON from the URL below and displays the results.

URL: https://s3.amazonaws.com/technical-challenge/v3/contacts.json

Contacts List View

* The contacts should be grouped into two sections: Favorite Contacts and Other Contacts. Within each section, they should be sorted alphabetically by name.
* Each cell should display the associated contact’s small image. Use the included
small placeholder image when appropriate.
* If a contact is a favorite, display a star emoji (⭐ ) in front of their name. The emoji might look different depending on your platform.
* Tapping on a contact should take the user to the detail page for that contact.


Contact Detail View

* Display the contact’s large image on this page. Use the included large placeholder image when appropriate.
* If the contact does not have a particular piece of information, that row should not appear on their detail page.
* In the top right corner, display a button that allows the user to favorite and unfavorite a contact. Use the “Favorite True” and “Favorite False” assets provided.
* When the user favorites or unfavorites a contact, the home page should be updated to reflect that change.


