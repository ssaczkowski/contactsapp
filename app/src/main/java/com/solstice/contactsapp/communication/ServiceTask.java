package com.solstice.contactsapp.communication;


import retrofit2.Response;

public class ServiceTask {

    public interface OnTaskFinishListener{
        void onSuccess(Response response);
        void onError(Throwable throwable);
    }

}
