package com.solstice.contactsapp.communication.services;

import com.solstice.contactsapp.communication.schema.Contact;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface DataService {

        @GET("technical-challenge/v3/contacts.json")
        Call<List<Contact>> getAllContacts();

}
