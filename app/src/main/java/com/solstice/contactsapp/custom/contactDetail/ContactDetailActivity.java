package com.solstice.contactsapp.custom.contactDetail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.solstice.contactsapp.R;
import com.solstice.contactsapp.communication.schema.Contact;
import com.solstice.contactsapp.custom.homePage.HomePageActivity;
import com.solstice.contactsapp.model.ItemDetail;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactDetailActivity extends AppCompatActivity {

    private Contact mContactSelected;

    @BindView(R.id.text_header_tv)
    TextView text_header_tv;
    @BindView(R.id.favorite_header_iv)
    ImageView favorite_header_iv;
    @BindView(R.id.profile_image_iv)
    ImageView profile_image_iv;
    @BindView(R.id.contact_name_tv)
    TextView contact_name_tv;
    @BindView(R.id.company_name_tv)
    TextView company_name_tv;
    @BindView(R.id.data_list_rv)
    RecyclerView data_list_rv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        getSupportActionBar().hide();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        mContactSelected = (Contact) bundle.getSerializable(HomePageActivity.CONTACT_ARG);

        ButterKnife.bind(this);

        setContactData();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ContactDetailActivity.this,LinearLayoutManager.VERTICAL,false);

        data_list_rv.setLayoutManager(linearLayoutManager);

        setDecorationList();

        ArrayList<ItemDetail> itemsDetailList = ContactDetailUtil.getItemsDetail(this ,mContactSelected);

        data_list_rv.setAdapter(new ContactDetailListAdapter(itemsDetailList));



        text_header_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        favorite_header_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mContactSelected.isIsFavorite()){

                    ContactDetailUtil.unFavorite(getApplicationContext(),mContactSelected);

                }else{

                    ContactDetailUtil.favorite(getApplicationContext(),mContactSelected);

                }
                favorite_header_iv.setImageResource(ContactDetailUtil.getIconStar(mContactSelected));
            }
        });

    }

    private void setDecorationList() {
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(data_list_rv.getContext(),
                ((LinearLayoutManager) data_list_rv.getLayoutManager()).getOrientation());
        data_list_rv.addItemDecoration(dividerItemDecoration);
    }

    private void setContactData() {
        contact_name_tv.setText(mContactSelected.getName());
        company_name_tv.setText(mContactSelected.getCompanyName());

        favorite_header_iv.setImageResource(ContactDetailUtil.getIconStar(mContactSelected));

        Picasso.get().load(mContactSelected.getLargeImageURL()).error(R.mipmap.ic_user).into(profile_image_iv);

    }
}
