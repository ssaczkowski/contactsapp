package com.solstice.contactsapp.custom.contactDetail;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.solstice.contactsapp.R;
import com.solstice.contactsapp.model.ItemDetail;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactDetailListAdapter extends RecyclerView.Adapter  {

    private List<ItemDetail> mItemDetailList;

    public ContactDetailListAdapter(List<ItemDetail> mItemDetailList) {
        this.mItemDetailList = mItemDetailList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View view = inflater.inflate(R.layout.item_contact_detail_list, viewGroup, false);

        return new ContactDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        ContactDetailViewHolder holder = (ContactDetailViewHolder) viewHolder;

        holder.bindItemDetail(mItemDetailList.get(i));

    }

    @Override
    public int getItemCount() {
        if(this.mItemDetailList != null){
            return this.mItemDetailList.size();
        }
        return 0;
    }

    public class ContactDetailViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tag_tv)
        TextView tag_tv;
        @BindView(R.id.extra_tag_tv)
        TextView extra_tag_tv;
        @BindView(R.id.value_tv)
        TextView value_tv;


        public ContactDetailViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this,itemView);

        }

        public void bindItemDetail(ItemDetail item){
            tag_tv.setText(item.getTag());
            extra_tag_tv.setText(item.getExtraTag());
            value_tv.setText(item.getValue());
        }
    }
}
