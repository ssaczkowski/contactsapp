package com.solstice.contactsapp.custom.contactDetail;

import android.content.Context;

import com.solstice.contactsapp.R;
import com.solstice.contactsapp.communication.schema.Address;
import com.solstice.contactsapp.communication.schema.Contact;
import com.solstice.contactsapp.communication.schema.Phone;
import com.solstice.contactsapp.model.ItemDetail;
import com.solstice.contactsapp.util.FormatUtil;
import com.solstice.contactsapp.util.SharedPreferencesApp;

import java.util.ArrayList;

class ContactDetailUtil {


    public static void unFavorite(Context applicationContext, Contact mContactSelected) {

        SharedPreferencesApp.removeFavorite(applicationContext, mContactSelected);
        mContactSelected.setIsFavorite(false);
        SharedPreferencesApp.addUnfavorite(applicationContext, mContactSelected);
    }

    public static void favorite(Context applicationContext, Contact mContactSelected) {
        SharedPreferencesApp.removeUnfavorite(applicationContext, mContactSelected);
        mContactSelected.setIsFavorite(true);
        SharedPreferencesApp.addFavorite(applicationContext, mContactSelected);
    }

    public static int getIconStar(Contact mContactSelected) {
        if (mContactSelected.isIsFavorite()) {
            return R.drawable.ic_star_true;
        } else {
            return R.drawable.ic_star_false;
        }
    }

    public static ArrayList<ItemDetail> getItemsDetail(Context context , Contact mContactSelected) {
        ArrayList<ItemDetail> returnList = new ArrayList<>();

        if (mContactSelected != null) {
            returnList = getPhones(context, returnList, mContactSelected);

            returnList = getAddress(returnList, mContactSelected);

            returnList = getBirthdate(context , returnList, mContactSelected);

            returnList = getEmail(returnList, mContactSelected);
        }

        return returnList;
    }

    private static ArrayList<ItemDetail> getBirthdate(Context context,ArrayList<ItemDetail> returnList, Contact mContactSelected) {

        if (mContactSelected.getAddress() != null) {
            String birthdate = FormatUtil.getFormatDate(context,mContactSelected.getBirthdate());
            ItemDetail item_birthdate = new ItemDetail("birthdate","",birthdate);
            returnList.add(item_birthdate);
        }

        return returnList;
    }

    private static ArrayList<ItemDetail> getEmail(ArrayList<ItemDetail> returnList, Contact mContactSelected) {

        if(mContactSelected.getEmailAddress() != null){
            ItemDetail item_email = new ItemDetail("email","",mContactSelected.getEmailAddress());
            returnList.add(item_email);
        }

        return returnList;
    }

    private static ArrayList<ItemDetail> getAddress(ArrayList<ItemDetail> returnList, Contact mContactSelected) {
        Address address = mContactSelected.getAddress();

        if (address != null) {
            ItemDetail address_item = new ItemDetail("address", "", FormatUtil.getFormatAddress(address));
            returnList.add(address_item);
        }

        return returnList;
    }

    private static ArrayList<ItemDetail> getPhones(Context context, ArrayList<ItemDetail> returnList, Contact mContactSelected) {
        Phone phone = mContactSelected.getPhone();

        if (phone.getWork() != null) {
            ItemDetail phone_work = new ItemDetail(
                    context.getString(R.string.phone_contact_detail_item),
                    context.getString(R.string.extra_tag_contact_detail_item_work),
                    FormatUtil.getFormatPhone(phone.getWork()));

            returnList.add(phone_work);
        }

        if (phone.getHome() != null) {
            ItemDetail phone_home = new ItemDetail(
                    context.getString(R.string.phone_contact_detail_item),
                    context.getString(R.string.extra_tag_contact_detail_item_home),
                    FormatUtil.getFormatPhone(phone.getHome()));

            returnList.add(phone_home);
        }

        if (phone.getMobile() != null) {
            ItemDetail phone_mobile = new ItemDetail(context.getString(R.string.phone_contact_detail_item),
                    context.getString(R.string.extra_tag_contact_detail_item_mobile),
                    FormatUtil.getFormatPhone(phone.getMobile()));

            returnList.add(phone_mobile);
        }

        return returnList;
    }
}
