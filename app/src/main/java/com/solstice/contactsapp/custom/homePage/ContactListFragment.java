package com.solstice.contactsapp.custom.homePage;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.solstice.contactsapp.R;
import com.solstice.contactsapp.communication.schema.Contact;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ContactListFragment extends Fragment {

    @BindView(R.id.list_rv)
    RecyclerView list_rv;
    @BindView(R.id.title_contact_list_tv)
    TextView title_contact_list_tv;

    private OnFragmentInteractionListener mListener;

    private static final String TITLE_ARG = "title";
    private String mTitle;
    private static final String CONTACT_LIST_ARG = "contactList";
    private ArrayList<Contact> mContactList;


    public ContactListFragment() {
    }

    public static ContactListFragment newInstance(String title, ArrayList<Contact> contactList) {
        ContactListFragment fragment = new ContactListFragment();

        Bundle args = new Bundle();
        args.putString(TITLE_ARG, title);
        args.putSerializable(CONTACT_LIST_ARG, contactList);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            mTitle = getArguments().getString(TITLE_ARG);
            mContactList = (ArrayList<Contact>) getArguments().getSerializable(CONTACT_LIST_ARG);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contact_list, container, false);

        ButterKnife.bind(this,view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);


        list_rv.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(list_rv.getContext(),
                ((LinearLayoutManager) list_rv.getLayoutManager()).getOrientation());
        list_rv.addItemDecoration(dividerItemDecoration);

        list_rv.setAdapter(new ContactsListAdapter(mContactList, (ContactsListAdapter.OnItemSelectedListener) getActivity()));

        title_contact_list_tv.setText(mTitle);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }
}
