package com.solstice.contactsapp.custom.homePage;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.solstice.contactsapp.R;
import com.solstice.contactsapp.communication.schema.Contact;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ContactsListAdapter extends RecyclerView.Adapter   {

    private List<Contact> mContactList;
    private OnItemSelectedListener mOnItemSelectedListener;

    public ContactsListAdapter(List<Contact> contactsList,OnItemSelectedListener onItemSelectedListener) {
        this.mContactList = contactsList;
        this.mOnItemSelectedListener = onItemSelectedListener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View view = inflater.inflate(R.layout.item_contact_list, viewGroup, false);

        return new ContactViewHolder(view,mOnItemSelectedListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        ContactViewHolder holder = (ContactViewHolder) viewHolder;

        holder.bindContact(this.mContactList.get(i));

    }

    @Override
    public int getItemCount() {
        if(this.mContactList != null){
            return this.mContactList.size();
        }
        return 0;
    }



    public class ContactViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_image_iv)
        ImageView profile_image_iv;
        @BindView(R.id.icon_star_iv)
        ImageView icon_star_iv;
        @BindView(R.id.contact_name_tv)
        TextView contact_name_tv;
        @BindView(R.id.company_name_tv)
        TextView company_name_tv;


        public ContactViewHolder(@NonNull final View itemView, final OnItemSelectedListener onItemSelectedListener) {
            super(itemView);

            ButterKnife.bind(this,itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onItemSelectedListener.onItemClick(mContactList.get(getAdapterPosition()));

                }
            });

        }

        public void bindContact(Contact contact) {

            Picasso.get().load(contact.getSmallImageURL()).error(R.mipmap.ic_user).into(profile_image_iv);

            if(contact.isIsFavorite()) {
                this.icon_star_iv.setImageResource(R.mipmap.ic_star);
            }

            this.company_name_tv.setText(contact.getCompanyName());
            this.contact_name_tv.setText(contact.getName());

        }

    }

    public interface OnItemSelectedListener {
        void onItemClick(Contact contact);
    }


}
