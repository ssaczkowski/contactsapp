package com.solstice.contactsapp.custom.homePage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.solstice.contactsapp.R;
import com.solstice.contactsapp.communication.ServiceTask;
import com.solstice.contactsapp.communication.schema.Contact;
import com.solstice.contactsapp.custom.contactDetail.ContactDetailActivity;
import com.solstice.contactsapp.util.SharedPreferencesApp;

import java.util.List;

import retrofit2.Response;

public class HomePageActivity extends AppCompatActivity implements ContactListFragment.OnFragmentInteractionListener , ContactsListAdapter.OnItemSelectedListener {

    public final static String CONTACT_ARG = "mContactSelected";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        getSupportActionBar().hide();

        HomePageUtil.callGetAllContactsService(new ServiceTask.OnTaskFinishListener() {
            @Override
            public void onSuccess(Response response) {

                SharedPreferencesApp.saveContactsByState(getApplicationContext(), (List<Contact>) response.body());

                HomePageUtil.loadFragmentFavoriteContacts(HomePageActivity.this);

                HomePageUtil.loadFragmentUnfavoriteContacts(HomePageActivity.this);

            }

            @Override
            public void onError(Throwable throwable) {
                Toast.makeText(HomePageActivity.this, R.string.error_system, Toast.LENGTH_SHORT).show();
            }
        });




    }

    @Override
    public void onFragmentInteraction() {

    }

    @Override
    public void onItemClick(Contact contact) {

        Intent intent = new Intent(this, ContactDetailActivity.class);

        Bundle bundle = new Bundle();
        bundle.putSerializable(CONTACT_ARG, contact);

        intent.putExtras(bundle);

        startActivity(intent);
    }


    @Override
    protected void onResume() {
        super.onResume();

        HomePageUtil.loadFragmentFavoriteContacts(this);

        HomePageUtil.loadFragmentUnfavoriteContacts(this);

    }
}
