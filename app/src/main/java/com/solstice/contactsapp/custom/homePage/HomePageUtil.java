package com.solstice.contactsapp.custom.homePage;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.solstice.contactsapp.R;
import com.solstice.contactsapp.communication.ServiceClient;
import com.solstice.contactsapp.communication.ServiceTask;
import com.solstice.contactsapp.communication.schema.Contact;
import com.solstice.contactsapp.communication.services.DataService;
import com.solstice.contactsapp.util.SharedPreferencesApp;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePageUtil {


    public static void callGetAllContactsService(final ServiceTask.OnTaskFinishListener listener){
        DataService service = ServiceClient.getRetrofitInstance().create(DataService.class);
        Call<List<Contact>> call = service.getAllContacts();
        call.enqueue(new Callback<List<Contact>>() {
            @Override
            public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {

                listener.onSuccess(response);
            }

            @Override
            public void onFailure(Call<List<Contact>> call, Throwable t) {

                listener.onError(t);
            }
        });
    }

    public static ArrayList<Contact> organizedAlphabeticList(ArrayList<Contact> list) {
        Collections.sort(list, new Comparator<Contact>() {
            Collator collator = Collator.getInstance();

            public int compare(Contact o1, Contact o2) {
                return collator.compare(o1.getName(), o2.getName());
            }
        });
        return list;
    }

    public static void loadFragmentFavoriteContacts(HomePageActivity activity) {

        FragmentManager managerFavorites = ((FragmentActivity) activity).getSupportFragmentManager();
        FragmentTransaction transactionFavorites = managerFavorites.beginTransaction();
        transactionFavorites.replace(R.id.frame_container_favorite, (new ContactListFragment()).newInstance("Favorite Contacts", SharedPreferencesApp.getFavorites(activity.getApplicationContext())));
        transactionFavorites.commit();
    }

    public static void loadFragmentUnfavoriteContacts(HomePageActivity activity) {

        FragmentManager managerUnfavorites = ((FragmentActivity) activity).getSupportFragmentManager();
        FragmentTransaction transactionUnfavorites = managerUnfavorites.beginTransaction();
        transactionUnfavorites.replace(R.id.frame_container_unfavorite, (new ContactListFragment()).newInstance("Other Contacts", SharedPreferencesApp.getUnfavorites(activity.getApplicationContext())));
        transactionUnfavorites.commit();
    }
}
