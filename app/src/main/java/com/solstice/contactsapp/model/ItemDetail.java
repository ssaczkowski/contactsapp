package com.solstice.contactsapp.model;

public class ItemDetail {

    private String tag;
    private String extraTag;
    private String value;

    public ItemDetail(String tag, String extraTag, String value) {
        this.tag = tag;
        this.extraTag = extraTag;
        this.value = value;
    }

    public String getTag() {
        return tag;
    }

    public String getExtraTag() {
        return extraTag;
    }

    public String getValue() {
        return value;
    }
}
