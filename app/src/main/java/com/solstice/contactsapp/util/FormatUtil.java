package com.solstice.contactsapp.util;

import android.content.Context;

import com.solstice.contactsapp.R;
import com.solstice.contactsapp.communication.schema.Address;

import java.util.HashMap;
import java.util.Map;

public class FormatUtil {

    private static Map<String,Integer> month = new HashMap<String, Integer>() {{
        put("01", R.string.january);
        put("02", R.string.february);
        put("03", R.string.march);
        put("04", R.string.april);
        put("05", R.string.may);
        put("06", R.string.june);
        put("07", R.string.july);
        put("08", R.string.august);
        put("09", R.string.september);
        put("10", R.string.october);
        put("11", R.string.november);
        put("12", R.string.december);
    }};


    public static String getFormatPhone(String phone) {

        String[] split = phone.split("-");

        String formatedPhone = "(" + split[0] + ") " + split[1] + "-" + split[2];

        return formatedPhone;
    }

    public static String getFormatAddress(Address address) {

        String formatedAddress = address.getStreet() + "\n" + address.getCity() + ", " + address.getState() +" "+ address.getZipCode() + ", " + address.getCountry();

        return formatedAddress;
    }

    public static String getFormatDate(Context context, String birthdate) {

        String[] split = birthdate.split("-");
        String formatedDate = context.getString(month.get(split[1])) + " " + split[2] +", " + split[0];

        return formatedDate;
    }

}
