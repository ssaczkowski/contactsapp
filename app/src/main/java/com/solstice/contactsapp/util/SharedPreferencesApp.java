package com.solstice.contactsapp.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;
import com.solstice.contactsapp.communication.schema.Contact;
import com.solstice.contactsapp.custom.homePage.HomePageUtil;

public class SharedPreferencesApp {

    public static final String PREFS_NAME = "CONTACTS_APP";
    public static final String FAVORITES = "Contact_Favorites";
    public static final String UNFAVORITES = "Contact_Unfavorites";

    public SharedPreferencesApp() {
        super();
    }

    public static void saveFavorites(Context context, List<Contact> favorites) {
        android.content.SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        ArrayList<Contact> organizedContactList = HomePageUtil.organizedAlphabeticList((ArrayList<Contact>) favorites);

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(organizedContactList);

        editor.putString(FAVORITES, jsonFavorites);

        editor.commit();
    }

    public static void addFavorite(Context context, Contact contact) {
        List<Contact> favorites = getFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<Contact>();
        favorites.add(contact);
        saveFavorites(context, favorites);
    }


    public static void removeFavorite(Context context, final Contact contact) {
        ArrayList<Contact> favorites = getFavorites(context);
        if (favorites != null) {

            favorites.removeIf(x -> x.getId().equals(contact.getId()));

            saveFavorites(context, favorites);
        }
    }

    public static void saveUnfavorites(Context context, List<Contact> unfavorites) {
        android.content.SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        ArrayList<Contact> organizedContactList = HomePageUtil.organizedAlphabeticList((ArrayList<Contact>) unfavorites);

        Gson gson = new Gson();
        String jsonUnfavorites = gson.toJson(organizedContactList);

        editor.putString(UNFAVORITES, jsonUnfavorites);

        editor.commit();
    }

    public static void removeUnfavorite(Context context, Contact contact) {
        ArrayList<Contact> unfavorites = getUnfavorites(context);
        if (unfavorites != null) {
            unfavorites.removeIf(x -> x.getId().equals(contact.getId()));
            saveUnfavorites(context, unfavorites);
        }
    }

    public static void addUnfavorite(Context context, Contact contact) {
        List<Contact> unfavorites = getUnfavorites(context);
        if (unfavorites == null)
            unfavorites = new ArrayList<Contact>();
        unfavorites.add(contact);
        saveUnfavorites(context, unfavorites);
    }


    public static ArrayList<Contact> getFavorites(Context context) {
        android.content.SharedPreferences settings;
        List<Contact> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            Contact[] favoriteItems = gson.fromJson(jsonFavorites,
                    Contact[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<Contact>(favorites);
        } else
            return null;

        return (ArrayList<Contact>) favorites;
    }

    public static ArrayList<Contact> getUnfavorites(Context context) {
        android.content.SharedPreferences settings;
        List<Contact> unfavorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(UNFAVORITES)) {
            String jsonUnfavorites = settings.getString(UNFAVORITES, null);
            Gson gson = new Gson();
            Contact[] favoriteItems = gson.fromJson(jsonUnfavorites,
                    Contact[].class);

            unfavorites = Arrays.asList(favoriteItems);
            unfavorites = new ArrayList<Contact>(unfavorites);

        } else
            return null;

        return (ArrayList<Contact>) unfavorites;
    }

    public static void saveContactsByState(Context context, List<Contact> list) {
        android.content.SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();


        ArrayList<Contact> favourite = new ArrayList<>();
        ArrayList<Contact> unfavourite = new ArrayList<>();

        if (list != null && !list.isEmpty()) {

            for (int i = 0; i < list.size(); i++) {

                if (list.get(i).isIsFavorite()) {
                    favourite.add(list.get(i));
                } else {
                    unfavourite.add(list.get(i));
                }

            }


            ArrayList<Contact> organizedContactListFavorite = HomePageUtil.organizedAlphabeticList(favourite);
            ArrayList<Contact> organizedContactListUnfavorite = HomePageUtil.organizedAlphabeticList(unfavourite);

            String jsonFavorites = gson.toJson(organizedContactListFavorite);
            String jsonUnfavorites = gson.toJson(organizedContactListUnfavorite);

            editor.putString(FAVORITES, jsonFavorites);
            editor.putString(UNFAVORITES, jsonUnfavorites);

            editor.commit();
        }

    }

}