package com.solstice.contactsapp.custom.homePage;

import com.solstice.contactsapp.communication.ServiceTask;
import com.solstice.contactsapp.communication.schema.Contact;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;


public class HomePageUtilTest {

    List<Contact> contacts;

    @Before
    public void initialize() {

    this.contacts = new ArrayList<>();
    Contact contact1 = new Contact();
    Contact contact2 = new Contact();
    Contact contact3 = new Contact();

    contact1.setName("Homer Simpson");
    contact2.setName("Lisa Simpson");
    contact3.setName("Bart Simpson");

    this.contacts.add(contact1);
    this.contacts.add(contact2);
    this.contacts.add(contact3);

    }

    @Test
    public void organizedAlphabeticList() {
        ArrayList<Contact> contactsOrganized = HomePageUtil.organizedAlphabeticList((ArrayList<Contact>) this.contacts);

        Assert.assertEquals("Bart Simpson",contactsOrganized.get(0).getName());
    }

    @Test
    public void serviceCall() {

        HomePageUtil.callGetAllContactsService(new ServiceTask.OnTaskFinishListener() {
            @Override
            public void onSuccess(Response response) {
                Assert.assertTrue(true);
            }

            @Override
            public void onError(Throwable throwable) {
                Assert.assertTrue(false);
            }
        });

    }

    @Test
    public void serviceCallNetError() {


        HomePageUtil.callGetAllContactsService(new ServiceTask.OnTaskFinishListener() {
            @Override
            public void onSuccess(Response response) {
                Assert.assertTrue(true);
            }

            @Override
            public void onError(Throwable throwable) {
                Assert.assertTrue(false);
            }
        });

    }
}